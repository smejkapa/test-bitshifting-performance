﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ShiftingCS
{
    unsafe struct BitField
    {
        static Random rnd = new Random();

        public fixed ulong Cells[5];
        public ulong[] Cells_m;

        public BitField(int nothing)
        {
            fixed (ulong* p = Cells)
            {
                for (int i = 0; i < 5; i++)
                {
                    p[i] = (ulong)rnd.Next(); // Just some random number
                }
            }
            Cells_m = new ulong[5];
            for (int i = 0; i < Cells_m.Length; i++)
            {
                Cells_m[i] = (ulong)rnd.Next();
            }
        }

        public void Left()
        {
            ulong carry = 0;
            ulong nextCarry = 0;
            fixed (ulong* p = Cells)
            {
                for (int i = 0; i < 5; i++)
                {
                    nextCarry = p[i] >> 63;
                    p[i] = p[i] << 1 | carry;
                    carry = nextCarry;
                }
            }
        }
        public void Right()
        {
            ulong carry = 0;
            ulong nextCarry = 0;
            fixed (ulong* p = Cells)
            {
                for (int i = 4; i >= 0; i--)
                {
                    carry = p[i] << 63;
                    p[i] = p[i] >> 1 | carry;
                    carry = nextCarry;
                }
            }
        }
        
        public void Left_m()
        {
            //ulong carry = 0;
            //ulong nextCarry = 0;

            //for (int i = 0; i < 5; i++)
            //{
            //    nextCarry = Cells_m[i] >> 63;
            //    Cells_m[i] = Cells_m[i] << 1 | carry;
            //    carry = nextCarry;
            //}

            ulong c0 = Cells_m[0];
            ulong c1 = Cells_m[1];
            ulong c2 = Cells_m[2];
            ulong c3 = Cells_m[3];
            ulong c4 = Cells_m[4];

            ulong carry0 = 0;
            ulong carry1 = c0 >> 63;
            ulong carry2 = c1 >> 63;
            ulong carry3 = c2 >> 63;
            ulong carry4 = c3 >> 63;

            Cells_m[0] = c0 << 1 | carry0;
            Cells_m[1] = c1 << 1 | carry1;
            Cells_m[2] = c2 << 1 | carry2;
            Cells_m[3] = c3 << 1 | carry3;
            Cells_m[4] = c4 << 1 | carry4;

            //nextCarry = c0 >> 63;
            //c0 = c0 << 1 | carry;
            //carry = nextCarry;
            //nextCarry = c1 >> 63;
            //c1 = c1 << 1 | carry;
            //carry = nextCarry;
            //nextCarry = c2 >> 63;
            //c2 = c2 << 1 | carry;
            //carry = nextCarry;
            //nextCarry = c3 >> 63;
            //c3 = c3 << 1 | carry;
            //carry = nextCarry;
            //c4 = c4 << 1 | carry;
            //Cells_m[0] = c0;
            //Cells_m[1] = c1;
            //Cells_m[2] = c2;
            //Cells_m[3] = c3;
            //Cells_m[4] = c4;

            //carry = 0;
            //nextCarry = Cells_m[0] >> 63;
            //Cells_m[0] = Cells_m[0] << 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[1] >> 63;
            //Cells_m[1] = Cells_m[1] << 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[2] >> 63;
            //Cells_m[2] = Cells_m[2] << 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[3] >> 63;
            //Cells_m[3] = Cells_m[3] << 1 | carry;
            //carry = nextCarry;
            //Cells_m[4] = Cells_m[4] << 1 | carry;
        }
        public void Right_m()
        {
            //ulong carry = 0;
            //ulong nextCarry = 0;

            //for (int i = 4; i >= 0; i--)
            //{
            //    carry = Cells_m[i] << 63;
            //    Cells_m[i] = Cells_m[i] >> 1 | carry;
            //    carry = nextCarry;
            //}

            ulong c0 = Cells_m[0];
            ulong c1 = Cells_m[1];
            ulong c2 = Cells_m[2];
            ulong c3 = Cells_m[3];
            ulong c4 = Cells_m[4];

            ulong carry0 = c1 << 63;
            ulong carry1 = c2 << 63;
            ulong carry2 = c3 << 63;
            ulong carry3 = c4 << 63;
            ulong carry4 = 0;

            Cells_m[0] = c0 >> 1 | carry0;
            Cells_m[1] = c1 >> 1 | carry1;
            Cells_m[2] = c2 >> 1 | carry2;
            Cells_m[3] = c3 >> 1 | carry3;
            Cells_m[4] = c4 >> 1 | carry4;

            //ulong c0 = Cells_m[0];
            //ulong c1 = Cells_m[1];
            //ulong c2 = Cells_m[2];
            //ulong c3 = Cells_m[3];
            //ulong c4 = Cells_m[4];
            //nextCarry = c4 << 63;
            //c4 = c4 >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = c3 << 63;
            //c3 = c3 >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = c2 << 63;
            //c2 = c2 >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = c1 << 63;
            //c1 = c1 >> 1 | carry;
            //carry = nextCarry;
            //c0 = c0 >> 1 | carry;
            //Cells_m[0] = c0;
            //Cells_m[1] = c1;
            //Cells_m[2] = c2;
            //Cells_m[3] = c3;
            //Cells_m[4] = c4;

            //carry = 0;
            //nextCarry = Cells_m[4] << 63;
            //Cells_m[4] = Cells_m[4] >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[3] << 63;
            //Cells_m[3] = Cells_m[3] >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[2] << 63;
            //Cells_m[2] = Cells_m[2] >> 1 | carry;
            //carry = nextCarry;
            //nextCarry = Cells_m[1] << 63;
            //Cells_m[1] = Cells_m[1] >> 1 | carry;
            //carry = nextCarry;
            //Cells_m[0] = Cells_m[0] >> 1 | carry;
        }
        public void StuffUnrolledManaged()
        {
            ulong carry = 0;
            ulong nextCarry = 0;
            for (int i = 0; i < 100000000; i++)
            {
                //left
                carry = 0;
                nextCarry = Cells_m[0] >> 63;
                Cells_m[0] = Cells_m[0] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] >> 63;
                Cells_m[1] = Cells_m[1] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] >> 63;
                Cells_m[2] = Cells_m[2] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] >> 63;
                Cells_m[3] = Cells_m[3] << 1 | carry;
                carry = nextCarry;
                Cells_m[4] = Cells_m[4] << 1 | carry;

                //left
                carry = 0;
                nextCarry = Cells_m[0] >> 63;
                Cells_m[0] = Cells_m[0] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] >> 63;
                Cells_m[1] = Cells_m[1] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] >> 63;
                Cells_m[2] = Cells_m[2] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] >> 63;
                Cells_m[3] = Cells_m[3] << 1 | carry;
                carry = nextCarry;
                Cells_m[4] = Cells_m[4] << 1 | carry;

                //left
                carry = 0;
                nextCarry = Cells_m[0] >> 63;
                Cells_m[0] = Cells_m[0] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] >> 63;
                Cells_m[1] = Cells_m[1] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] >> 63;
                Cells_m[2] = Cells_m[2] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] >> 63;
                Cells_m[3] = Cells_m[3] << 1 | carry;
                carry = nextCarry;
                Cells_m[4] = Cells_m[4] << 1 | carry;

                //right
                carry = 0;
                nextCarry = Cells_m[4] << 63;
                Cells_m[4] = Cells_m[4] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] << 63;
                Cells_m[3] = Cells_m[3] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] << 63;
                Cells_m[2] = Cells_m[2] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] << 63;
                Cells_m[1] = Cells_m[1] >> 1 | carry;
                carry = nextCarry;
                Cells_m[0] = Cells_m[0] >> 1 | carry;

                //right
                carry = 0;
                nextCarry = Cells_m[4] << 63;
                Cells_m[4] = Cells_m[4] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] << 63;
                Cells_m[3] = Cells_m[3] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] << 63;
                Cells_m[2] = Cells_m[2] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] << 63;
                Cells_m[1] = Cells_m[1] >> 1 | carry;
                carry = nextCarry;
                Cells_m[0] = Cells_m[0] >> 1 | carry;

                //left
                carry = 0;
                nextCarry = Cells_m[0] >> 63;
                Cells_m[0] = Cells_m[0] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] >> 63;
                Cells_m[1] = Cells_m[1] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] >> 63;
                Cells_m[2] = Cells_m[2] << 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] >> 63;
                Cells_m[3] = Cells_m[3] << 1 | carry;
                carry = nextCarry;
                Cells_m[4] = Cells_m[4] << 1 | carry;

                //right
                carry = 0;
                nextCarry = Cells_m[4] << 63;
                Cells_m[4] = Cells_m[4] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] << 63;
                Cells_m[3] = Cells_m[3] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] << 63;
                Cells_m[2] = Cells_m[2] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] << 63;
                Cells_m[1] = Cells_m[1] >> 1 | carry;
                carry = nextCarry;
                Cells_m[0] = Cells_m[0] >> 1 | carry;

                //right
                carry = 0;
                nextCarry = Cells_m[4] << 63;
                Cells_m[4] = Cells_m[4] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[3] << 63;
                Cells_m[3] = Cells_m[3] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[2] << 63;
                Cells_m[2] = Cells_m[2] >> 1 | carry;
                carry = nextCarry;
                nextCarry = Cells_m[1] << 63;
                Cells_m[1] = Cells_m[1] >> 1 | carry;
                carry = nextCarry;
                Cells_m[0] = Cells_m[0] >> 1 | carry;

            }
        }
        /**/
        public void StuffUnrolledFixed()
        {
            //Debugger.Break();
            //ulong u0 = Cells[0];
            //ulong u1 = Cells[1];
            //ulong u2 = Cells[2];
            //ulong u3 = Cells[3];
            //ulong u4 = Cells[4];
            ulong carry = 0;
            ulong nextCarry = 0;
            fixed(ulong* p = Cells)
            {
                for (int i = 0; i < 100000000; i++)
                {

                    //left
                    carry = 0;
                    nextCarry = p[0] >> 63;
                    p[0] = p[0] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] >> 63;
                    p[1] = p[1] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] >> 63;
                    p[2] = p[2] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] >> 63;
                    p[3] = p[3] << 1 | carry;
                    carry = nextCarry;
                    p[4] = p[4] << 1 | carry;

                    //left
                    carry = 0;
                    nextCarry = p[0] >> 63;
                    p[0] = p[0] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] >> 63;
                    p[1] = p[1] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] >> 63;
                    p[2] = p[2] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] >> 63;
                    p[3] = p[3] << 1 | carry;
                    carry = nextCarry;
                    p[4] = p[4] << 1 | carry;

                    //left
                    carry = 0;
                    nextCarry = p[0] >> 63;
                    p[0] = p[0] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] >> 63;
                    p[1] = p[1] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] >> 63;
                    p[2] = p[2] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] >> 63;
                    p[3] = p[3] << 1 | carry;
                    carry = nextCarry;
                    p[4] = p[4] << 1 | carry;

                    //right
                    carry = 0;
                    nextCarry = p[4] << 63;
                    p[4] = p[4] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] << 63;
                    p[3] = p[3] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] << 63;
                    p[2] = p[2] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] << 63;
                    p[1] = p[1] >> 1 | carry;
                    carry = nextCarry;
                    p[0] = p[0] >> 1 | carry;

                    //right
                    carry = 0;
                    nextCarry = p[4] << 63;
                    p[4] = p[4] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] << 63;
                    p[3] = p[3] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] << 63;
                    p[2] = p[2] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] << 63;
                    p[1] = p[1] >> 1 | carry;
                    carry = nextCarry;
                    p[0] = p[0] >> 1 | carry;

                    //left
                    carry = 0;
                    nextCarry = p[0] >> 63;
                    p[0] = p[0] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] >> 63;
                    p[1] = p[1] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] >> 63;
                    p[2] = p[2] << 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] >> 63;
                    p[3] = p[3] << 1 | carry;
                    carry = nextCarry;
                    p[4] = p[4] << 1 | carry;

                    //right
                    carry = 0;
                    nextCarry = p[4] << 63;
                    p[4] = p[4] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] << 63;
                    p[3] = p[3] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] << 63;
                    p[2] = p[2] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] << 63;
                    p[1] = p[1] >> 1 | carry;
                    carry = nextCarry;
                    p[0] = p[0] >> 1 | carry;

                    //right
                    carry = 0;
                    nextCarry = p[4] << 63;
                    p[4] = p[4] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[3] << 63;
                    p[3] = p[3] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[2] << 63;
                    p[2] = p[2] >> 1 | carry;
                    carry = nextCarry;
                    nextCarry = p[1] << 63;
                    p[1] = p[1] >> 1 | carry;
                    carry = nextCarry;
                    p[0] = p[0] >> 1 | carry;

                }
            }
            //Cells[0] = u0;
            //Cells[1] = u1;
            //Cells[2] = u2;
            //Cells[3] = u3;
            //Cells[4] = u4;
            //Debugger.Break();
        }
        /**/
        public void StuffUnrolledNonManaged()
        {
            //Debugger.Break();
            ulong u0;
            ulong u1;
            ulong u2;
            ulong u3;
            ulong u4;
            fixed (ulong *p = Cells)
            {
                u0 = p[0];
                u1 = p[1];
                u2 = p[2];
                u3 = p[3];
                u4 = p[4];
            }
            ulong carry = 0;
            ulong nextCarry = 0;

            for (int i = 0; i < 100000000; i++)
            {

                //left
                carry = 0;
                nextCarry = u0 >> 63;
                u0 = u0 << 1 | carry;
                carry = nextCarry;
                nextCarry = u1 >> 63;
                u1 = u1 << 1 | carry;
                carry = nextCarry;
                nextCarry = u2 >> 63;
                u2 = u2 << 1 | carry;
                carry = nextCarry;
                nextCarry = u3 >> 63;
                u3 = u3 << 1 | carry;
                carry = nextCarry;
                u4 = u4 << 1 | carry;

                //left
                carry = 0;
                nextCarry = u0 >> 63;
                u0 = u0 << 1 | carry;
                carry = nextCarry;
                nextCarry = u1 >> 63;
                u1 = u1 << 1 | carry;
                carry = nextCarry;
                nextCarry = u2 >> 63;
                u2 = u2 << 1 | carry;
                carry = nextCarry;
                nextCarry = u3 >> 63;
                u3 = u3 << 1 | carry;
                carry = nextCarry;
                u4 = u4 << 1 | carry;

                //left
                carry = 0;
                nextCarry = u0 >> 63;
                u0 = u0 << 1 | carry;
                carry = nextCarry;
                nextCarry = u1 >> 63;
                u1 = u1 << 1 | carry;
                carry = nextCarry;
                nextCarry = u2 >> 63;
                u2 = u2 << 1 | carry;
                carry = nextCarry;
                nextCarry = u3 >> 63;
                u3 = u3 << 1 | carry;
                carry = nextCarry;
                u4 = u4 << 1 | carry;

                //right
                carry = 0;
                nextCarry = u4 << 63;
                u4 = u4 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u3 << 63;
                u3 = u3 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u2 << 63;
                u2 = u2 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u1 << 63;
                u1 = u1 >> 1 | carry;
                carry = nextCarry;
                u0 = u0 >> 1 | carry;

                //right
                carry = 0;
                nextCarry = u4 << 63;
                u4 = u4 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u3 << 63;
                u3 = u3 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u2 << 63;
                u2 = u2 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u1 << 63;
                u1 = u1 >> 1 | carry;
                carry = nextCarry;
                u0 = u0 >> 1 | carry;

                //left
                carry = 0;
                nextCarry = u0 >> 63;
                u0 = u0 << 1 | carry;
                carry = nextCarry;
                nextCarry = u1 >> 63;
                u1 = u1 << 1 | carry;
                carry = nextCarry;
                nextCarry = u2 >> 63;
                u2 = u2 << 1 | carry;
                carry = nextCarry;
                nextCarry = u3 >> 63;
                u3 = u3 << 1 | carry;
                carry = nextCarry;
                u4 = u4 << 1 | carry;

                //right
                carry = 0;
                nextCarry = u4 << 63;
                u4 = u4 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u3 << 63;
                u3 = u3 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u2 << 63;
                u2 = u2 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u1 << 63;
                u1 = u1 >> 1 | carry;
                carry = nextCarry;
                u0 = u0 >> 1 | carry;

                //right
                carry = 0;
                nextCarry = u4 << 63;
                u4 = u4 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u3 << 63;
                u3 = u3 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u2 << 63;
                u2 = u2 >> 1 | carry;
                carry = nextCarry;
                nextCarry = u1 << 63;
                u1 = u1 >> 1 | carry;
                carry = nextCarry;
                u0 = u0 >> 1 | carry;

            }

            fixed (ulong* p = Cells)
            {
                p[0] = u0;
                p[1] = u1;
                p[2] = u2;
                p[3] = u3;
                p[4] = u4;
            }
           //Debugger.Break();
        }
    /**/
    }

    class Program
    {

        static void Main(string[] args)
        {
            //FixedVsNo();
            //return;

            BitField bf = new BitField(1);
            Stopwatch sw = new Stopwatch();

            // Call to remove the compilation time from measurements
            bf.Left();
            bf.Right();
            bf.Left_m();
            bf.Right_m();
            bf.StuffUnrolledFixed();
            bf.StuffUnrolledNonManaged();
            bf.StuffUnrolledManaged();

            //Debugger.Break();

            sw.Start();
            for (int i = 0; i < 100000000; i++)
            {
                bf.Left();
                bf.Left();
                bf.Left();
                bf.Right();
                bf.Right();
                bf.Left();
                bf.Right();
                bf.Right();
            }
            sw.Stop();

            Console.WriteLine($"Function calls fixed in: {sw.Elapsed.TotalMilliseconds.ToString()}ms");

            sw.Restart();
            for (int i = 0; i < 100000000; i++)
            {
                bf.Left_m();
                bf.Left_m();
                bf.Left_m();
                bf.Right_m();
                bf.Right_m();
                bf.Left_m();
                bf.Right_m();
                bf.Right_m();
            }
            sw.Stop();

            Console.WriteLine($"Function calls managed in: {sw.Elapsed.TotalMilliseconds.ToString()}ms");

            sw.Restart();
            bf.StuffUnrolledManaged();
            sw.Stop();

            Console.WriteLine($"Unrolled managed in: {sw.Elapsed.TotalMilliseconds.ToString()}ms");

            sw.Restart();
            bf.StuffUnrolledFixed();
            sw.Stop();
            Console.WriteLine($"Unrolled fixed in: {sw.Elapsed.TotalMilliseconds.ToString()}ms");



            sw.Restart();
            bf.StuffUnrolledNonManaged();
            sw.Stop();

            Console.WriteLine($"Local variable unrolled in: {sw.Elapsed.TotalMilliseconds.ToString()}ms");
        }

        unsafe struct BF
        {
            public fixed int Arr[5];
        }

        static unsafe void FixedVsNo()
        {
            int[] A = new int[5];
            A[1] = 1;

            Debugger.Break();
            int x = A[1] << 1;
            int y = A[1] << 2;
            Debugger.Break();


            BF bf = new BF();
            bf.Arr[1] = 1;

            Debugger.Break();
            int t = bf.Arr[1];

            int c = t << 2;
            int d = t << 3;
            Debugger.Break();

            
            Debugger.Break();
            int* p = bf.Arr;

            int a = p[1] << 1;
            int a1 = p[1] << 4;
            Debugger.Break();



            int b = c + d + a + a1 + x + y;
            Console.WriteLine(b);
        }
    }
}
