#include <iostream>
#include <chrono>
using namespace std;
using namespace std::chrono;

class BitField
{
private:
	unsigned long long LEFTMOST_BIT = 0x8000000000000000;
	unsigned long long RIGHTMOST_BIT = 1;

public:
	unsigned long long Cells_l[5];
	BitField()
	{
		for (size_t i = 0; i < 5; i++)
		{
			//Cells_l[i] = 0x123afe341546fe; // Just some random number
			Cells_l[i] = rand(); // Just some random number
		}
	}
	void Left()
	{
		unsigned long long carry = 0;
		unsigned long long nextCarry = 0;
		for (int i = 0; i < 5; i++)
		{
			nextCarry = (Cells_l[i] & LEFTMOST_BIT) >> 63;
			Cells_l[i] = Cells_l[i] << 1 | carry;
			carry = nextCarry;
		}
	}
	void Right()
	{
		unsigned long long carry = 0;
		unsigned long long nextCarry = 0;
		for (int i = 4; i >= 0; i--)
		{
			nextCarry = (Cells_l[i] & RIGHTMOST_BIT) << 63;
			Cells_l[i] = Cells_l[i] >> 1 | carry;
			carry = nextCarry;
		}
	}
	void Stuff()
	{
		unsigned long long u0 = Cells_l[0];
		unsigned long long u1 = Cells_l[1];
		unsigned long long u2 = Cells_l[2];
		unsigned long long u3 = Cells_l[3];
		unsigned long long u4 = Cells_l[4];
		unsigned long long carry = 0;
		unsigned long long nextCarry = 0;

		for (int i = 0; i < 100000000; i++)
		{

			//left
			carry = 0;
			nextCarry = u0 >> 63;
			u0 = u0 << 1 | carry;
			carry = nextCarry;
			nextCarry = u1 >> 63;
			u1 = u1 << 1 | carry;
			carry = nextCarry;
			nextCarry = u2 >> 63;
			u2 = u2 << 1 | carry;
			carry = nextCarry;
			nextCarry = u3 >> 63;
			u3 = u3 << 1 | carry;
			carry = nextCarry;
			u4 = u4 << 1 | carry;

			//left
			carry = 0;
			nextCarry = u0 >> 63;
			u0 = u0 << 1 | carry;
			carry = nextCarry;
			nextCarry = u1 >> 63;
			u1 = u1 << 1 | carry;
			carry = nextCarry;
			nextCarry = u2 >> 63;
			u2 = u2 << 1 | carry;
			carry = nextCarry;
			nextCarry = u3 >> 63;
			u3 = u3 << 1 | carry;
			carry = nextCarry;
			u4 = u4 << 1 | carry;

			//left
			carry = 0;
			nextCarry = u0 >> 63;
			u0 = u0 << 1 | carry;
			carry = nextCarry;
			nextCarry = u1 >> 63;
			u1 = u1 << 1 | carry;
			carry = nextCarry;
			nextCarry = u2 >> 63;
			u2 = u2 << 1 | carry;
			carry = nextCarry;
			nextCarry = u3 >> 63;
			u3 = u3 << 1 | carry;
			carry = nextCarry;
			u4 = u4 << 1 | carry;

			//right
			carry = 0;
			nextCarry = u4 << 63;
			u4 = u4 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u3 << 63;
			u3 = u3 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u2 << 63;
			u2 = u2 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u1 << 63;
			u1 = u1 >> 1 | carry;
			carry = nextCarry;
			u0 = u0 >> 1 | carry;

			//right
			carry = 0;
			nextCarry = u4 << 63;
			u4 = u4 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u3 << 63;
			u3 = u3 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u2 << 63;
			u2 = u2 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u1 << 63;
			u1 = u1 >> 1 | carry;
			carry = nextCarry;
			u0 = u0 >> 1 | carry;

			//left
			carry = 0;
			nextCarry = u0 >> 63;
			u0 = u0 << 1 | carry;
			carry = nextCarry;
			nextCarry = u1 >> 63;
			u1 = u1 << 1 | carry;
			carry = nextCarry;
			nextCarry = u2 >> 63;
			u2 = u2 << 1 | carry;
			carry = nextCarry;
			nextCarry = u3 >> 63;
			u3 = u3 << 1 | carry;
			carry = nextCarry;
			u4 = u4 << 1 | carry;

			//right
			carry = 0;
			nextCarry = u4 << 63;
			u4 = u4 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u3 << 63;
			u3 = u3 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u2 << 63;
			u2 = u2 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u1 << 63;
			u1 = u1 >> 1 | carry;
			carry = nextCarry;
			u0 = u0 >> 1 | carry;

			//right
			carry = 0;
			nextCarry = u4 << 63;
			u4 = u4 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u3 << 63;
			u3 = u3 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u2 << 63;
			u2 = u2 >> 1 | carry;
			carry = nextCarry;
			nextCarry = u1 << 63;
			u1 = u1 >> 1 | carry;
			carry = nextCarry;
			u0 = u0 >> 1 | carry;

		}

		Cells_l[0] = u0;
		Cells_l[1] = u1;
		Cells_l[2] = u2;
		Cells_l[3] = u3;
		Cells_l[4] = u4;

	}
};

void FixedVsNo()
{

}

int main()
{
	int Arr[5];
	Arr[1] = 1;

	int t = Arr[1];

	int c = t << 2;
	int d = t << 3;


	int a = Arr[1] << 1;
	int a1 = Arr[1] << 4;


	int b = c + d + a + a1;
	cout << b << endl;
	//return 0;

	BitField bf;

	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (int i = 0; i < 100000000; i++)
	{
		bf.Left();
		bf.Left();
		bf.Left();
		bf.Right();
		bf.Right();
		bf.Left();
		bf.Right();
		bf.Right();
	}
	//bf.Stuff();
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	auto duration = duration_cast<milliseconds>(t2 - t1).count();

	cout << "Time: " << duration << endl << endl;
	for (size_t i = 0; i < 5; i++)
	{
		cout << bf.Cells_l[i] << endl;
	}

	return 0;
}

