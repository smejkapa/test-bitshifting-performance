# What is this

Testing the performance of bit shifting and array access in C++ versus different approaches to the same problem in C#. Origin of this problem was my entry to the tetris competition on theaigamedev.com. The bit field was used to represent the tetris board - full vs empty squares.